resource "helm_release"  "momo-secret" {
   name = "momo-secret"
   chart = "helm/"
   set {
      name="env.docker_config"
      value=var.docker_registry_config
   }
}
