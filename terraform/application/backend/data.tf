data "terraform_remote_state" "momo_cluster" {
  backend = "http"

  config = {
    address = "${var.remote_state_address}/k8s.tfstate"
    username = var.gitlab_username
    password = var.access_token
  }
}

data "selectel_mks_kubeconfig_v1" "kubeconfig" {
  cluster_id = data.terraform_remote_state.momo_cluster.outputs.momo_cluster_id
  project_id = var.project_id
  region     = var.region
}