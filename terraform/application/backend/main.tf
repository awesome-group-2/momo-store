terraform {
    required_providers {
        selectel    = {
        source  = "selectel/selectel"
        version = "4.0.1"
    }
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.53.0"
    }
  }
  required_version = ">= 0.13"
  backend "http" {}
}

provider "helm" {
  kubernetes {
     host = data.selectel_mks_kubeconfig_v1.kubeconfig.server
     client_certificate = base64decode(data.selectel_mks_kubeconfig_v1.kubeconfig.client_cert)
     client_key = base64decode(data.selectel_mks_kubeconfig_v1.kubeconfig.client_key)
     cluster_ca_certificate = base64decode(data.selectel_mks_kubeconfig_v1.kubeconfig.cluster_ca_cert)
  }
}
provider "selectel" {
    domain_name = var.domain_name
    username    = var.user_name
    password    = var.password
}

provider "openstack" {
  auth_url    = "https://api.selvpc.ru/identity/v3"
  domain_name = var.domain_name
  tenant_id   = var.project_id
  user_name   = var.user_name
  password    = var.password
  region      = var.region
}