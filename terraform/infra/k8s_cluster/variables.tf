variable domain_name {
    type = string
    sensitive = true
}
variable project_id {
    type = string
    sensitive = true
}
variable user_name {
    type = string
    sensitive = true
}
variable password {
    type = string
    sensitive = true
}
variable network_name {
    type = string
}
variable region {
    type = string
}
variable router_name {
    type = string
}
variable cluster_name {
    type = string
}
variable firewall_name {
    type = string
}
variable gitlab_username {
    type = string
}
variable access_token {
    type = string
}
variable remote_state_address {
    type = string
}

