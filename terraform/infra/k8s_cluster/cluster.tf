resource "selectel_mks_cluster_v1" "momo_cluster" {
  name                              = var.cluster_name
  project_id                        = var.project_id
  region                            = var.region
  kube_version                      = data.selectel_mks_kube_versions_v1.versions.default_version
  network_id                        = data.openstack_networking_network_v2.momo_network.id
  subnet_id                        = data.openstack_networking_subnet_v2.momo_subnet.id
  zonal                             = true
  enable_patch_version_auto_upgrade = false
}