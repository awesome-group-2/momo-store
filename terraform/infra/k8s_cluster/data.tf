data "selectel_mks_kube_versions_v1" "versions" {
  project_id = var.project_id
  region     = var.region
}

data "openstack_networking_network_v2" "momo_network" {
    tenant_id = var.project_id
    name = var.network_name
}
data "openstack_networking_subnet_v2" "momo_subnet" {
    tenant_id = var.project_id
    network_id = data.openstack_networking_network_v2.momo_network.id
}