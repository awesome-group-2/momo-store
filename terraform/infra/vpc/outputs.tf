output "momo_network_id" {
    value = openstack_networking_network_v2.momo_network.id
}
output "momo_subnet_id" {
    value = openstack_networking_subnet_v2.momo_subnet.id
}
