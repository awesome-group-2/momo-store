resource "openstack_networking_network_v2" "momo_network" {
  name           = var.network_name
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "momo_subnet" {
  network_id = openstack_networking_network_v2.momo_network.id
  cidr       = "192.168.198.0/23"
}

data "openstack_networking_network_v2" "external_network_1" {
  external = true
}

resource "openstack_networking_router_v2" "momo_router" {
  name                = "router"
  external_network_id = data.openstack_networking_network_v2.external_network_1.id
}

resource "openstack_networking_router_interface_v2" "momo_interface_1" {
  router_id = openstack_networking_router_v2.momo_router.id
  subnet_id = openstack_networking_subnet_v2.momo_subnet.id
}

resource "openstack_fw_rule_v2" "momo_firewall_rule_1" {
  name     = "allow-all-outgoing-traffic"
  action   = "allow"
}
resource "openstack_fw_rule_v2" "momo_firewall_rule_2" {
  name     = "allow-node-ports"
  action   = "allow"
  destination_port = "30000:32767"
  protocol = "tcp"
}
resource "openstack_fw_rule_v2" "momo_firewall_rule_3" {
  name     = "allow-web-port"
  action   = "allow"
  destination_port = "80"
  protocol = "tcp"
}
resource "openstack_fw_policy_v2" "momo_firewall_policy_1" {
  name        = "egress-firewall-policy"
  audited     = true
  rules       = [
    openstack_fw_rule_v2.momo_firewall_rule_1.id
  ]
}
resource "openstack_fw_policy_v2" "momo_firewall_policy_2" {
  name        = "ingress-firewall-policy"
  audited     = true
  rules       = [
    openstack_fw_rule_v2.momo_firewall_rule_2.id,
    openstack_fw_rule_v2.momo_firewall_rule_3.id
  ]
}
resource "openstack_fw_group_v2" "momo_firewall" {
  name                       = var.firewall_name
  admin_state_up             = true
  egress_firewall_policy_id  = openstack_fw_policy_v2.momo_firewall_policy_1.id
  ingress_firewall_policy_id  = openstack_fw_policy_v2.momo_firewall_policy_2.id
   ports                      = [
       openstack_networking_router_interface_v2.momo_interface_1.port_id,
   ]
}