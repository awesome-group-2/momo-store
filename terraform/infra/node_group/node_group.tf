resource "selectel_mks_nodegroup_v1" "momo_node_group" {
  cluster_id        = data.terraform_remote_state.momo_cluster.outputs.momo_cluster_id
  project_id        = var.project_id
  region            = var.region
  availability_zone = "ru-3b"
  nodes_count       = 1
  cpus              = 2
  ram_mb            = 2048
  volume_gb         = 20
  volume_type       = "fast.ru-3b"
  labels            = {
    "label-key0": "label-value0",
    "label-key1": "label-value1",
    "label-key2": "label-value2",
  }
}