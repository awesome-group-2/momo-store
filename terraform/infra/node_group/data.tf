
data "terraform_remote_state" "momo_cluster" {
  backend = "http"

  config = {
    address = "${var.remote_state_address}/k8s.tfstate"
    username = var.gitlab_username
    password = var.access_token
  }
}